pipeline{
    
    agent {
        node "master"
    }
    
    environment{
        REPOGIT="http://192.168.88.10:3000/root/gittest.git"
        HOMEDIR=pwd()
    }
    
    options{
        timestamps()
        timeout(time: 10, unit:"SECONDS")
    }
    
    parameters{
        choice(name: "ENV", choices: ['DEV', 'HOMOLOG', 'PRODUCTION'], description :"Defina um valor do ambiente")
        string(name: "FILENAME", defaultValue: "filename.txt", description: "Arquivo texto de teste")
    }
    
    stages{
        stage("Variaveis de ambiente"){
            steps{
                sh "env"
                echo "${BUILD_ID}"
            }
        }
        
        stage("Repositório GIT"){
            steps{
                sh "git ls-remote ${REPOGIT}"
                git "${REPOGIT}"
            }
        }   
        
        stage("Criando arquivo"){
            when{
                environment name: 'ENV', value: 'PRODUCTION'
            }
            steps{
                sh "ls -l > ${HOMEDIR}/${params.FILENAME}"
            }
        }
        
    }
}